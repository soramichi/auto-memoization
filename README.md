auto-memoization
================

What is it
----------------
Automatic memoization using LRU cache for C++

How to use
----------------
1. place auto_memo.hpp and cache.hpp in your include directory
2. include auto_memo.hpp from your source
3. use auto_memo class to automatic memoize functions

<pre><code>
#include "auto_memo.hpp"
...

char hoge(std::string, int){
    ...
}

int main(){
    ...

    // auto_memo&lt;cache_size, type_of_result_value, types_of_argments, ...&gt; memoized_function(original_function);
    //
    // automatic memoization of hoge with cache size 3
    // the results are transparently memoized, all you have to do is just replace hoge by hoge2
    auto_memo&lt;3, char, std::string, int&gt; hoge2(hoge);

    char c1 = hoge2("hello", 3); // after here all the calls of hoge2("hello", 3) immediately returns the result
    ...                          // until it is swapped out from the LRU cache 
    ... 
}
</code></pre>

Remarks
----------------
- Your compiler must support the variadic template feature
- Function obejects and lambda functions are not yet supported.
- Just a preliminary implementation. It may include critiacal bugs.