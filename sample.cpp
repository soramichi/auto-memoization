#include <iostream>
#include "auto_memo.hpp"

using namespace std;

char hoge(string str, int n){
  sleep(1);
  return str.at(n);
}

int main(int argc, char* argv[]){
  // automatic memoization with cache size 3
  auto_memo<3, char, string, int> hoge2(hoge);

  /*
  // this takes 3*3 = 9 seconds
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      cout << hoge("yes", j) << endl;
    }
  }
  */

  // this takes 3 seconds only
  // (the results for hoge are memoized autmatically)
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++){
      cout << hoge2("yes", j) << endl;
    }
  }

}
