#if !defined(_AUTO_MEMO_HPP)
#define _AUTO_MEMO_HPP

#include <iostream>
#include <cstdlib>
#include <tuple>
#include "cache.hpp"

template <int size, typename R, typename T0, typename ... Types> class auto_memo{
private:
  cache<std::tuple<T0, Types...>, R> c;
  R (*f)(T0, Types...);
public:
  auto_memo(R (*_f)(T0, Types...)) : f(_f), c(cache<std::tuple<T0, Types...>, R>(size)){
    ;
  }

  R operator()(T0 arg0, Types ... args){
    R* ptr = c.fetch(std::tuple<T0, Types...>(arg0, args...));

    if(ptr != NULL){
      return *ptr;
    }
    else{
      R v = f(arg0, args...);
      c.set(v);
      return v;
    }	 
  }
};

#endif
