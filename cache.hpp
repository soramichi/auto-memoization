#if !defined(_CACHE_HPP)
#define _CACHE_HPP

#include <list>
#include <iostream>
#include <algorithm>

template <typename T0, typename T1> class cache{
private:
  const int max_len;
  struct data_t{
    T0 index;
    T1 value;
  }; 
  std::list<data_t> data;
public:
  int hit, miss;

  cache(int _max_len) : max_len(_max_len){ 
    hit = miss = 0;
  }

  void print() const{
    std::for_each(data.begin(), data.end(), [](data_t d)->void { std::cout << d.index << "->" << d.value << " "; });
    std::cout << "(hit: " << hit << ", miss: " << miss << ")" << std::endl;
  }

  T1* fetch(T0 p){
    auto it = std::find_if(data.begin(), data.end(), [p](data_t d)->bool { return d.index==p; });

    // miss
    if(it == data.end()){
      miss++;

      data_t newdata;
      newdata.index = p;
      newdata.value = T1(0);

      // the cache is not full
      if(data.size() < max_len){
	data.push_front(newdata);
      }
      // the cache is full
      else{
	data.pop_back();
	data.push_front(newdata);
      }

      return NULL;
    }

    // hit
    else{
      hit++;
      
      data_t replaced = *it;
      data.erase(it);
      data.push_front(replaced);

      return (new T1(replaced.value));
    }
  }

  void set(T1 value){
    data.begin()->value = value;
  }
};

#endif
